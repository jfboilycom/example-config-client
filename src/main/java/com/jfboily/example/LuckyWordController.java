package com.jfboily.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jfboily on 16-10-22.
 */
@RestController
public class LuckyWordController {
    @Value("${lucky-word}")
    private String luckyword;

    @RequestMapping("/luckyword")
    public String getLuckyWord() {
        return "The lucky word is : " + luckyword;
    }
}
